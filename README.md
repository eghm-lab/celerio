## Celerio Code Generator

[![Build Status](https://travis-ci.org/jaxio/celerio.svg)](https://travis-ci.org/jaxio/celerio)

Celerio is a code generator tool for data-driven application.

It can reverse a database schema and generate advanced crud-based applications.

For more info, please read [Celerio Documentation](http://www.jaxio.com/documentation/celerio)

## How to use it ?

Have already Maven 3.1.1 or above and Java 1.8 installed ?

Simply execute:

    mvn com.jaxio.celerio:bootstrap-maven-plugin:4.0.4:bootstrap

## License

Celerio is released under version 2.0 of the [Apache License][].

[Apache License]: http://www.apache.org/licenses/LICENSE-2.0
